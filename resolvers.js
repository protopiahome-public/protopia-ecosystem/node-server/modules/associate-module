import { associateHandler } from './associate';

const { query } = require('nact');

module.exports = {

  Mutation: {

    associate: async (obj, args, ctx, info) => associateHandler(args.input, ctx),

  },
  Query: {

  },
};
